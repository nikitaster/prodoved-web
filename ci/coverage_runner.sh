#!/bin/bash

python manage.py makemigrations && python manage.py migrate
coverage run manage.py test
coverage report --skip-empty -m WEB_App/views.py WEB_App/models.py WEB_App/forms.py Modules/base_classes.py Modules/requests_to_api.py
coverage html --skip-empty WEB_App/views.py WEB_App/models.py WEB_App/forms.py Modules/base_classes.py Modules/requests_to_api.py
mkdir public
mv htmlcov public/coverage

exit 0
