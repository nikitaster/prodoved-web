# Changelog

## Pre-release version


### Changes:

* Migrate part of backend to API
* Redesign all pages(make them more responsive and comfortable to futher development)
* Write docs for users and developers
* Migrate to https standart
* Add search(products and categories) on main and profile pages
* Add cookies alert, make privacy policy and terms of use pages
* Make about us page
* Finally make contact form works correctly
* Refactor backend, standardize it via Pylint and Pycodestyle preferences


### Works features:

* Search by photo on main page
* A lot of categories
* Information about product
* Reviews and rates for products
* Gallery for every product(site users make it with their photos) with ability to rate photo
* Profile, where you can see all information
* User docs on all pages
* Ability to change information and avatars for users
* Authorized users can contact with administrations via comfortable form


### Team, that works with than stage:

* Gudkov Nikita - main devloper, migrate backend, make search
* Tkachev Andrew - main developer, write all docs, redesign and refactor back- and frontend, add cookies and all new pages
* Kharchenko Vladislav - backend developer, make contact us page and its backend, write tests
* Mukhin Andrey - backend developer, copy user docs to main site
* Marinin Eugeniy - frontend developer, make main page and fix issues with responsiveness
