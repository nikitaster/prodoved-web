"""
Это файл для тестирования Django-приложения
"""
from django.test import TestCase, Client
from django.contrib.auth.models import User, Permission
from django.urls import reverse
from WEB_App.models import *


class TestIndex(TestCase):
    """
    Класс для тестирования главной страницы сайта
    """
    fixtures = ['db.json']

    def setUp(self):
        """
        Базовая настройка тестов для страницы

        :return: None
        """
        self.client = Client()

    def test_none_authorised_user_photo_scan_try(self):
        """
        Тест на неавторизованного пользователя

        :return: None
        """
        response = self.client.post('/', {'file-input-for-label': 'fixtures/test.jpg'},
                                    follow=True, **{'wsgi.url_scheme': 'https'})
        self.assertEqual(response.request['PATH_INFO'], '/')
        self.assertEqual(response.status_code, 200)

    def test_get(self):
        response = self.client.get('/', **{'wsgi.url_scheme': 'https'})
        self.assertTemplateUsed(response, 'main/index.html')
        self.assertEqual(response.status_code, 200)

    def test_find(self):
        response = self.client.post('/', {'action': 'initial_searcher'}, **{'wsgi.url_scheme': 'https'})
        self.assertEqual(response.status_code, 200)

    def test_login_by_username(self):
        response = self.client.post('/', data={'status': 'SignIn', 'identification': 'test', 'password': 'test'},
                                    **{'wsgi.url_scheme': 'https'})
        self.assertEqual(response.status_code, 200)

    def test_login_by_email(self):
        response = self.client.post('/', data={'status': 'SignIn', 'identification': 'test@mail.ru',
                                               'password': 'test'}, **{'wsgi.url_scheme': 'https'})
        self.assertEqual(response.status_code, 200)

    def test_failing_login(self):
        response = self.client.post('/', data={'status': 'SignIn', 'identification': 'test_not_found',
                                               'password': 'test'}, **{'wsgi.url_scheme': 'https'})
        self.assertEqual(response.status_code, 200)

    def test_sign_up(self):
        response = self.client.post('/', data={'status': 'SignUp',
                                               'username': 'newtest', 'email': 'newtest@mail.ru',
                                               'password': 'Pa$$w0rd', 'password2': 'Pa$$w0rd'},
                                    **{'wsgi.url_scheme': 'https'})
        self.assertEqual(response.status_code, 200)


class TestProductPage(TestCase):
    """
    Класс для тестирования страницы продукта
    """
    fixtures = ['db.json']

    def setUp(self):
        """
        Базовая настройка тестов для страницы

        :return: None
        """
        self.not_logged_client = Client()
        self.client = Client()
        self.user = User.objects.get(username='test')

    def test_get(self):
        self.client.force_login(self.user)
        response = self.client.get('/product/Schweppes/', **{'wsgi.url_scheme': 'https'})
        self.assertTemplateUsed(response, 'product/product.html')

    def test_not_auth_post(self):
        response = self.not_logged_client.post('/product/Schweppes/', **{'wsgi.url_scheme': 'https'})
        self.assertTemplateUsed(response, 'product/product.html')

    def test_get_not_auth(self):
        response = self.client.get('/product/Schweppes/', **{'wsgi.url_scheme': 'https'})
        self.assertTemplateUsed(response, 'product/product.html')

    def test_rate_photo(self):
        """
        Тестирование оценки товара

        :return: None
        """
        perm = Permission.objects.get(codename='add_rate')
        self.user.user_permissions.add(perm)
        self.client.force_login(user=self.user)
        response0 = self.client.post('/product/Schweppes/', {'rating': 0}, **{'wsgi.url_scheme': 'https'})
        response2_5 = self.client.post('/product/Schweppes/', {'rating': 2.5}, **{'wsgi.url_scheme': 'https'})
        response5 = self.client.post('/product/Schweppes/', {'rating': 5}, **{'wsgi.url_scheme': 'https'})

        self.assertGreater(len(Rate.objects.filter(rating=0)), 0)
        self.assertEqual(response0.request['PATH_INFO'], '/product/Schweppes/')

        self.assertGreater(len(Rate.objects.filter(rating=2.5)), 0)
        self.assertEqual(response2_5.request['PATH_INFO'], '/product/Schweppes/')

        self.assertGreater(len(Rate.objects.filter(rating=5)), 0)
        self.assertEqual(response5.request['PATH_INFO'], '/product/Schweppes/')

    def test_comment_photo(self):
        """
        Тест на комментирование товара

        :return: None
        """
        perm_comment = Permission.objects.get(codename='add_comment')
        perm_child = Permission.objects.get(codename='add_childrencomment')
        self.user.user_permissions.add(perm_comment)
        self.user.user_permissions.add(perm_child)
        self.client.force_login(user=self.user)

        response_comment = self.client.post('/product/Schweppes/', {'comment': 'test'}, **{'wsgi.url_scheme': 'https'})

        response_child = self.client.post('/product/Schweppes/',
                                          {'response-to-comment': 'test',
                                           'comment_id': 1},
                                          **{'wsgi.url_scheme': 'https'})

        self.assertGreater(len(Comment.objects.filter(text='test')), 0)
        self.assertEqual(response_comment.request['PATH_INFO'], '/product/Schweppes/')

        self.assertGreater(len(ChildrenComment.objects.filter(text='test')), 0)
        self.assertEqual(response_child.request['PATH_INFO'], '/product/Schweppes/')

    def tearDown(self):
        pass


class TestGalleryPage(TestCase):
    """
    Класс для тестирования галереи
    """
    fixtures = ['db.json']

    def setUp(self):
        """
        Базовая настройка тестов для страницы

        :return: None
        """
        self.not_logged_client = Client()
        self.client = Client()
        user = User.objects.get(username='test')
        self.client.force_login(user)

    def test_404(self):
        try:
            response = self.client.get(reverse('gallery'), **{'wsgi.url_scheme': 'https'})
            self.assertEqual(True, False)
        except:
            self.assertEqual(True, True)

    def test_get(self):
        response = self.client.get(reverse('gallery', kwargs={'good': 'Schweppes'}), **{'wsgi.url_scheme': 'https'})
        self.assertEqual(response.status_code, 200)

    def test_not_logged_get(self):
        response = self.not_logged_client.get(reverse('gallery', kwargs={'good': 'Schweppes'}), **{'wsgi.url_scheme': 'https'})
        self.assertEqual(response.status_code, 200)

    def test_post(self):
        response = self.client.post(reverse('gallery', kwargs={'good': 'Schweppes'}), **{'wsgi.url_scheme': 'https'})
        self.assertEqual(response.status_code, 200)


class TestCategoryPages(TestCase):
    """
    Класс для тестирования страницы категорий
    """
    fixtures = ['db.json']

    def setUp(self):
        """
        Базовая настройка тестов для страницы

        :return: None
        """
        self.not_logged_client = Client()
        self.client = Client()
        user = User.objects.get(username='test')
        self.client.force_login(user)

    def test_get(self):
        response = self.client.get(reverse('category', kwargs={'category': 'carbonated_drinks'}),
                                   **{'wsgi.url_scheme': 'https'})
        self.assertEqual(response.status_code, 200)

    def test_get_first(self):
        response = self.client.get('/category/', **{'wsgi.url_scheme': 'https'})
        self.assertEqual(response.status_code, 200)

    def test_not_logged_get(self):
        response = self.not_logged_client.get(reverse('category', kwargs={'category': 'carbonated_drinks'}),
                                              **{'wsgi.url_scheme': 'https'})
        self.assertEqual(response.status_code, 200)

    def test_not_logged_get_first(self):
        response = self.not_logged_client.get('/category/', **{'wsgi.url_scheme': 'https'})
        self.assertEqual(response.status_code, 200)

    def test_not_logged_post(self):
        response = self.not_logged_client.post(reverse('category', kwargs={'category': 'carbonated_drinks'}),
                                               **{'wsgi.url_scheme': 'https'})
        self.assertEqual(response.status_code, 200)

    def test_not_logged_post_first(self):
        response = self.not_logged_client.post('/category/', **{'wsgi.url_scheme': 'https'})
        self.assertEqual(response.status_code, 200)

    def test_post(self):
        response = self.client.post('/category/', kwargs={'category': 'carbonated_drinks'},
                                    **{'wsgi.url_scheme': 'https'})
        self.assertEqual(response.status_code, 200)

    def test_post_first(self):
        response = self.client.post('/category/', **{'wsgi.url_scheme': 'https'})
        self.assertEqual(response.status_code, 200)

    def test_post_category_delete(self):
        response = self.client.post('/category/', data={"action": "category_delete"},
                                    **{'wsgi.url_scheme': 'https'})
        self.assertEqual(response.status_code, 200)

    def test_post_category_change(self):
        response = self.client.post('/category/', data={"action": "category_change"},
                                    **{'wsgi.url_scheme': 'https'})
        self.assertEqual(response.status_code, 200)

    def test_post_add_good(self):
        response = self.client.post('/category/', data={"action": "add_good"},
                                    **{'wsgi.url_scheme': 'https'})
        self.assertEqual(response.status_code, 200)

    def test_post_create_category(self):
        response = self.client.post('/category/', data={"action": "create_category"},
                                    **{'wsgi.url_scheme': 'https'})
        self.assertEqual(response.status_code, 200)


class TestRecoveryPassword(TestCase):
    """
    Класс для тестирования страницы восстановления пароля
    """
    fixtures = ['db.json']

    def setUp(self):
        """
        Базовая настройка тестов для страницы

        :return: None
        """
        self.client = Client()
        self.user = User.objects.get(username='test')

    def test_recovery(self):
        response = self.client.post('/recovery_password/', {'start_procedure': 'test'}, **{'wsgi.url_scheme': 'https'})
        self.assertEqual(response.status_code, 200)

        recovery = Recovery.objects.get(target_user=self.user)
        recovery.code = 'ABC'
        recovery.save()

        response = self.client.post('/recovery_password/', {'code': 'ABC'}, **{'wsgi.url_scheme': 'https'})
        self.assertEqual(response.status_code, 200)

        response = self.client.post('/recovery_password/', {'password': 'Hell0W0rld', 'password2': 'Hell0W0rld'},
                                    **{'wsgi.url_scheme': 'https'})
        self.assertEqual(response.status_code, 200)

        response = self.client.post('/recovery_password/', {'finish': 'finish'},
                                    **{'wsgi.url_scheme': 'https'})
        self.assertEqual(response.status_code, 200)


class TestProfilePage(TestCase):
    """
    Класс для тестирования страницы профиля
    """
    fixtures = ['db.json']

    def setUp(self):
        """
        Базовая настройка тестов для страницы

        :return: None
        """
        self.client = Client()
        user = User.objects.get(username='test')
        self.client.force_login(user)

    def test_get(self):
        response = self.client.get('/profile/', **{'wsgi.url_scheme': 'https'})
        self.assertEqual(response.status_code, 200)

    def test_post(self):
        response = self.client.post('/profile/', **{'wsgi.url_scheme': 'https'})
        self.assertEqual(response.status_code, 200)

    def test_change_password(self):
        response = self.client.post('/profile/', {'old_password': 'test',
                                                  'new_password': 'test', 'new_password2': 'test'},
                                    **{'wsgi.url_scheme': 'https'})
        self.assertEqual(response.status_code, 200)


class TestComplaintListPage(TestCase):
    """
    Класс для тестирования страницы списка сообщений администрации
    """
    fixtures = ['db.json']

    def setUp(self):
        """
        Базовая настройка тестов для страницы

        :return: None
        """
        self.client = Client()
        user = User.objects.get(username='test')
        self.client.force_login(user)

    def test_get(self):
        response = self.client.get('/admin/complaint_list', **{'wsgi.url_scheme': 'https'})
        self.assertEqual(response.status_code, 302)

    def test_post(self):
        response = self.client.post('/admin/complaint_list', **{'wsgi.url_scheme': 'https'})
        self.assertEqual(response.status_code, 200)


class TestComplaintPage(TestCase):
    """
    Класс для тестирования страницы списка сообщений администрации
    """
    fixtures = ['db.json']

    def setUp(self):
        """
        Базовая настройка тестов для страницы

        :return: None
        """
        self.client = Client()
        self.user = User.objects.get(username='test')
        self.client.force_login(self.user)

    def test_get(self):
        response = self.client.get('/complaint/', **{'wsgi.url_scheme': 'https'})
        self.assertEqual(response.status_code, 200)

    def test_send(self):
        response = self.client.post('/complaint/',  {'title': '123', 'text': 'complaint'},
                                    **{'wsgi.url_scheme': 'https'}, follow=True)
        complaint = Complaint.objects.get(user=self.user)
        self.assertGreater(Complaint.objects.filter(user=self.user).count(), 0)
        self.assertEqual(response.status_code, 200)

        response = self.client.get('/admin/complaint_list', **{'wsgi.url_scheme': 'https'})
        self.assertEqual(response.status_code, 302)

        response = self.client.post('/admin/complaint_list', data={
            'complaint-id': complaint.id,
            'text': 'checked'
        }, **{'wsgi.url_scheme': 'https'}, follow=True)
        self.assertEqual(response.status_code, 200)


class TestAcceptPages(TestCase):
    """
    Класс для тестирования страницы списка сообщений администрации
    """
    fixtures = ['db.json']

    def setUp(self):
        """
        Базовая настройка тестов для страницы

        :return: None
        """
        self.client = Client()
        self.user = User.objects.get(username='test')
        self.client.force_login(self.user)

    def test_get(self):
        response = self.client.get('/admin/accept/', **{'wsgi.url_scheme': 'https'})
        self.assertEqual(response.status_code, 302)

    def test_post(self):
        response = self.client.post('/admin/accept/', **{'wsgi.url_scheme': 'https'})
        self.assertEqual(response.status_code, 200)

    def test_post_accept(self):
        response = self.client.post('/admin/accept/', data={'action': 'accept'},
                                    **{'wsgi.url_scheme': 'https'})
        self.assertEqual(response.status_code, 200)

    def test_post_accept(self):
        response = self.client.post('/admin/accept/', data={'action': 'accept'},
                                    **{'wsgi.url_scheme': 'https'})
        self.assertEqual(response.status_code, 200)

    def test_post_deny(self):
        response = self.client.post('/admin/accept/', data={'action': 'deny'},
                                    **{'wsgi.url_scheme': 'https'})
        self.assertEqual(response.status_code, 200)

    def test_post_create_category(self):
        response = self.client.post('/admin/accept/', data={'action': 'create_category'},
                                    **{'wsgi.url_scheme': 'https'})
        self.assertEqual(response.status_code, 200)

    def test_get_photo(self):
        response = self.client.get('/admin/accept_photo/', **{'wsgi.url_scheme': 'https'})
        self.assertEqual(response.status_code, 302)

    def test_post_photo(self):
        response = self.client.post('/admin/accept_photo/', **{'wsgi.url_scheme': 'https'})
        self.assertEqual(response.status_code, 200)

    def test_post_photo_deny(self):
        response = self.client.post('/admin/accept_photo/', data={'action': 'deny', 'picture_id': '1'},
                                    **{'wsgi.url_scheme': 'https'})
        self.assertEqual(response.status_code, 200)

    def test_post_photo_accept(self):
        response = self.client.post('/admin/accept_photo/', data={'action': 'accept', 'picture_id': '1'},
                                    **{'wsgi.url_scheme': 'https'})
        self.assertEqual(response.status_code, 200)


class TestAdminLoginPage(TestCase):
    """
    Класс для тестирования страницы списка сообщений администрации
    """
    fixtures = ['db.json']

    def setUp(self):
        """
        Базовая настройка тестов для страницы

        :return: None
        """
        self.client = Client()
        self.user = User.objects.get(username='test')
        self.client.force_login(self.user)

    def test_get(self):
        response = self.client.get('/admin/admin_login/', **{'wsgi.url_scheme': 'https'})
        self.assertEqual(response.status_code, 200)

    def test_post(self):
        response = self.client.post('/admin/admin_login/', data={'username': 'web', 'password': 'testtest'},
                                    **{'wsgi.url_scheme': 'https'})
        self.assertEqual(response.status_code, 200)


class TestAddProductPage(TestCase):
    """
    Класс для тестирования страницы доабвления товара
    """
    fixtures = ['db.json']

    def setUp(self):
        """
        Базовая настройка тестов для страницы

        :return: None
        """
        self.not_logged_client = Client()
        self.client = Client()
        self.user = User.objects.get(username='test')
        self.client.force_login(self.user)

    def test_get(self):
        response = self.client.get('/add_product/', **{'wsgi.url_scheme': 'https'})
        self.assertEqual(response.status_code, 200)

    def test_not_logged_get(self):
        response = self.not_logged_client.get('/add_product/', **{'wsgi.url_scheme': 'https'})
        self.assertEqual(response.status_code, 200)

    def test_post(self):
        response = self.client.post('/add_product/', **{'wsgi.url_scheme': 'https'})
        self.assertEqual(response.status_code, 302)

    def test_not_logged_post(self):
        response = self.not_logged_client.post('/add_product/', **{'wsgi.url_scheme': 'https'})
        self.assertEqual(response.status_code, 200)


class TestPrivacyPolicyPage(TestCase):
    """
    Класс для тестирования страницы доабвления товара
    """
    fixtures = ['db.json']

    def setUp(self):
        """
        Страница политик

        :return: None
        """
        self.not_logged_client = Client()
        self.client = Client()
        self.user = User.objects.get(username='test')
        self.client.force_login(self.user)

    def test_get_privacy_policy(self):
        response = self.not_logged_client.get('/privacy_policy/', **{'wsgi.url_scheme': 'https'})
        self.assertEqual(response.status_code, 200)

    def test_get_terms_of_use(self):
        response = self.not_logged_client.get('/terms_of_use/', **{'wsgi.url_scheme': 'https'})
        self.assertEqual(response.status_code, 200)

